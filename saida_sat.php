<?php

		$saleNumberSale = '';
		$saleCPFCPNJCustomer = '';
		$saleDate = '';
		$sqlSale = $this->sales_model->loadRecordSaleId($id);

		$valFinal1 = 0;
		$valFinal2 = 0;
		$valFinal3 = 0;
		$valFinal4 = 0;

	    foreach($sqlSale as $rowSale){
	    	$valFinal1 = $rowSale['venta_final_subtotal'];
			$valFinal2 = $rowSale['venta_final_descuento'];
			$valFinal3 = $rowSale['venta_final_garcon'];
			$valFinal4 = $rowSale['venta_final_neto'];
			$valFinal5 = $rowSale['venta_entrega'];

	    	$saleNumberSale = stripslashes($rowSale['venta_numero']);
			$typeId = stripslashes($rowSale['mov_id']);
			$cpfNfp = stripslashes($rowSale['nota_paulista_cpf_cnpj']);
			$saleDate = date('d/m/Y - H:i:d', strtotime($rowSale['venta_fecha_hora']));
	    	$sqlCustomer = $this->sales_model->loadCustomerId($rowSale['cliente_id']);
	    	foreach($sqlCustomer as $rowCustomer){
	    		$saleCPFCPNJCustomer = stripslashes($rowCustomer['cliente_numero_identidad']);
	    	}
	    }
	    $myRazonSocial = '';
	    $myAddress = '';
	    $myCity = '';
	    $myState = '';
	    $myZip = '';
	    $myCNPJ = '';
	    $myLogo = '';
	    $myData = $this->sales_model->checkAxxionAccount($this->company);
	    $myCompany = $this->sales_model->checkMyCompanyTicket($this->company);
		$cRegtrib = "";
	    if($myCompany == 'SN' || trim($myCompany)==''){
	    	$cRegtrib = 1; //simples nacional
	    }else{
	    	$cRegtrib = 3; //regime normal
	    }
		
		$cnpjValue					= "";
		$ieValue					= "";
		$imValue 					= "";
		$codigoVinculacion			= "";
		$empresa_definida			= "";
		$cRegTribISSQN				= "";
		$indRatISSQN				= "";
		$contador_porcentaje_fijo	= "";
		$cupomReduzido				= "";
		
		$sqlCont = $this->sales_model->loadContrato($this->company);
	    foreach($sqlCont as $rowCont){
			$cnp = substr(stripslashes($rowCont['cnpjValue']),0,14);
			$ieb = stripslashes($rowCont['ieValue']);
			$imb = stripslashes($rowCont['imValue']);
			$sga = stripslashes($rowCont['codigoVinculacion']);
			$cRegTribISSQN = stripslashes($rowCont['cRegTribISSQN']);
			$indRatISSQN = stripslashes($rowCont['indRatISSQN']);
			$PItem12741	= stripslashes($rowCont['contador_porcentaje_fijo']);
			$PItem12741 = (trim($PItem12741)=='') ? 10 : $PItem12741;
			$c_red = stripslashes($rowCont['cupomReduzido']);
		}
		
		/*if(trim($cpfNfp) != "")
		{
			$sqlClie = $this->sales_model->customerCpf(trim($cpfNfp));
	    	foreach($sqlCont as $rowCont){
				$xNome		= $rowCont['cliente_razon_social'];
				$xLgr		= $rowCont['cliente_direccion'];
				$nro		= $rowCont['cliente_num'];
				$xCpl		= $rowCont['cliente_complemento'];
				$xBairro	= $rowCont['cliente_bairro'];
				$xMun		= $rowCont['cliente_municipio'];
				$UF			= $rowCont['cliente_uf'];;
			}
		}
		else
		{*/
			$xNome	= "";
			$xLgr	= "";
			$nro	= "";
			$xCpl	= "";
			$xBairro= "";
			$xMun	= "";
			$UF		= "";
		/*}*/
		
		$cci = "[";
		$ccf = "]";
		$fim = chr(13);
		
		$cnpp = $cnp;
		
		////////////////////////////////////////////////////////////////////////////////
		//////////// CABEÇALHO
		$xml_saida = $cci."infCFe".$ccf.$fim.
		"versao=".$valy.$fim.
		$cci."Identificacao".$ccf.$fim.
		"CNPJ=".$cnpp.$fim.
		"signAC=".$sga.$fim.
		"numeroCaixa=001".$fim.
		$cci."Emitente".$ccf.$fim.
		"CNPJ=".$cnp.$fim.
		"IE=".$ieb.$fim.
		"IM=".$fim.
		"cRegTrib=".$cRegtrib.$fim.
		"cRegTribISSQN=".$cRegTribISSQN.$fim.
		"indRatISSQN=".$indRatISSQN.$fim.
		$cci."Destinatario".$ccf.$fim.
		"CNPJCPF=".trim($cpfNfp).$fim.
		"xNome=".$xNome.$fim.
		$cci."Entrega".$ccf.$fim.
		"xLgr=".$xLgr.$fim.
		"nro=".$nro.$fim.
		"xCpl=".$xCpl.$fim.
		"xBairro=".$xBairro.$fim.
		"xMun=".$xMun.$fim.
		"UF=".$UF.$fim;
		
		$sqlDetailSale = $this->sales_model->loadRecordSaleDetail($id);
		$item   = 0;
		$itemP  = 0;
		$bruto  = 0;
		$v_des  = 0;
		$infoad = "";
		foreach($sqlDetailSale as $rowDetailSale){
			$item  = $item+1;
			$item3 = $this->leftZero(3, $item);
			$productId = $rowDetailSale['product_ecommerce_id'];
			$i_ncm = "47061000";
			$i_cfop = "";
			$i_pis = "";
			$i_cofins = "";
			$i_issqn = "";
			$i_origem = "";
			$i_icms = "";
			$i_picms = "";
			$i_dct = 0;
			$dataProduct = $this->sales_model->loadProductId($productId);
			foreach($dataProduct as $rowProduct){
				$itemP ++;
				if($rowProduct['product_master_id'] > 0){
					$masterBarcode = $this->sales_model->loadBarcodeMaster($rowProduct['product_master_id']);
					foreach($masterBarcode as $rowMasterBarcode){
						$unit = $rowMasterBarcode['product_master_unit'];
						$productCode = stripslashes($rowMasterBarcode['product_master_barcode']);
						
						## N C M 
						$i_ncm = stripslashes($rowMasterBarcode['product_master_ncm']);
						if(trim($i_ncm)=="")
						{
							echo "<script>Alert('Cadastro $productCode sem NCM')</script>";
                            
						}
						else if(strlen($i_ncm) < 8)
						{
							$qtdZer = 8-strlen($i_ncm);
							$zer    = "";
							for($in=0 ;$in<$qtdZer ; $in++)
							{
								$zer .= "0";
							}
							$i_ncm = $zer.$i_ncm;
						}
						$i_cfop = stripslashes($rowMasterBarcode['product_master_cfop']);
						$i_cfop = (trim($i_cfop)=='') ? "5405" : $i_cfop;
						$i_pis = stripslashes($rowMasterBarcode['product_master_pis']);
						$i_pis = (trim($i_pis)=='') ? "49" : $i_pis;
						$i_cofins = stripslashes($rowMasterBarcode['product_master_cofins']);
						$i_cofins = (trim($i_cofins)=='') ? "49" : $i_cofins;
						$i_issqn = stripslashes($rowMasterBarcode['product_master_issqn']);
						$infoad .= stripslashes($rowMasterBarcode['product_master_adinfo'])." ";
						$i_origem = stripslashes($rowMasterBarcode['product_master_origem']);
						$i_origem = (trim($i_origem)=='') ? "1" : $i_origem;
						$i_icms = stripslashes($rowMasterBarcode['product_master_icms']);
						$i_icms = (trim($i_icms)=='') ? "500" : $i_icms;
						$i_picms = stripslashes($rowMasterBarcode['product_master_picms']);
						$i_picms = (trim($i_picms)=='') ? "F" : $i_picms;
					}
					$masterName = $this->sales_model->loadProductDesciption($rowProduct['product_master_id']);
					foreach($masterName as $rowMasterName){
						$i_des = stripslashes($rowMasterName['product_description_name']);
					}
				}else{
					//$productCode = $rowProduct['product_ecommerce_code'];
					//$i_des = $rowProduct['product_ecommerce_name'];
					$unit = $rowProduct['product_ecommerce_price'];
					$productCode = stripslashes($rowProduct['product_ecommerce_code']);
					
					## N C M 
					$i_ncm = stripslashes($rowProduct['product_ecommerce_ncm']);
					if(trim($i_ncm)=="")
					{
						echo "<script>Alert('Cadastro $productCode sem NCM')</script>";
						
					}
					else if(strlen($i_ncm) < 8)
					{
						$qtdZer = 8-strlen($i_ncm);
						$zer    = "";
						for($in=0 ;$in<$qtdZer ; $in++)
						{
							$zer .= "0";
						}
						$i_ncm = $zer.$i_ncm;
					}
					$i_cfop = stripslashes($rowProduct['product_ecommerce_cfop']);
					$i_cfop = (trim($i_cfop)=='') ? "5405" : $i_cfop;
					$i_pis = stripslashes($rowProduct['product_ecommerce_pis']);
					$i_pis = (trim($i_pis)=='') ? "49" : $i_pis;
					$i_cofins = stripslashes($rowProduct['product_ecommerce_cofins']);
					$i_cofins = (trim($i_cofins)=='') ? "49" : $i_cofins;
					$i_issqn = stripslashes($rowProduct['product_ecommerce_issqn']);
					$infoad .= stripslashes($rowProduct['product_ecommerce_adinfo'])." ";
					$i_origem = stripslashes($rowProduct['product_ecommerce_origem']);
					$i_origem = (trim($i_origem)=='') ? "1" : $i_origem;
					$i_icms = stripslashes($rowProduct['product_ecommerce_icms']);
					$i_icms = (trim($i_icms)=='') ? "500" : $i_icms;
					$i_picms = stripslashes($rowProduct['product_ecommerce_picms']);
					$i_picms = (trim($i_picms)=='') ? "F" : $i_picms;
					
					$i_des = stripslashes($rowProduct['product_ecommerce_name']);
				}
				$i_cod = ($c_red=='N' || trim($c_red)=='') ? $productCode : $item;
				$i_des = ($c_red=='N' || trim($c_red)=='') ? $i_des : substr($i_des,0,40);
				if(strlen($i_des) < 40)
				{
					$esp = "";
					for($e=0;$e<40-strlen($i_des);$e++)
					{
						$esp .= " ";
					}
					$i_des = $i_des.$esp;
				}
			}
			
			$i_vun = number_format($rowDetailSale['precio_venta'],2,".","");
			$i_unm = $rowDetailSale['detalle_unidad'];
			$i_unm = (trim($i_unm)=='' || $i_unm=='o -') ? "UN" : $i_unm;
			$i_unm = str_replace(" ","",$i_unm);
			$i_unm = str_replace("-","",$i_unm);
			$i_qtd = number_format($rowDetailSale['detalle_cantidad'],4,".","");
			$i_dct += ($itemP==$item) ? $rowDetailSale['detalle_descuento'] : 0;
			$v_des += ($itemP==$item) ? $i_dct : 0;
			$i_ean = $productCode;
			$i_des = $this->removeAcento($i_des);
			$i_des = $this->removeChars($i_des);
			$i_des = str_replace(".","",$i_des);//não pode ser no removeChars pois infoadicional usa .
			$i_tot = number_format($i_vun*$i_qtd,2,".","");
			$bruto += $i_tot;
			$vItem12741 = (trim($PItem12741)=='') ? 0.00 : ($i_tot*$PItem12741)/100;
			
			////////////////////////////////////////////////////////////////////////////////
			////////////PRODUTOS
			$xml_saida .= $cci."Produto".$item3.$ccf.$fim.
			"cProd=".$i_cod.$fim.
			"infAdProd=".$fim.
			"cEAN=".$fim.
			"xProd=".$i_des.$fim.
			"NCM=".$i_ncm.$fim.
			"CFOP=".$i_cfop.$fim.
			"uCom=".$i_unm.$fim.
			"Combustivel=0".$fim.
			"qCom=".$i_qtd.$fim.
			"vUnCom=".$i_vun.$fim.
			"indRegra=A".$fim.
			"vDesc=".$i_dct.$fim.
			"vItem12741=".$vItem12741.$fim;
			
			$icm_oc  = array('102','300','500','40','41','50','60');
			$icm_ocp = array('900','00','20','90');
			
			////////////////////////////////////////////////////////////////////////////////
			//////////// ICMS
			$xml_saida .= $cci."ICMS".$item3.$ccf.$fim;
			if(in_array($i_icms,$icm_oc))
			{
				$xml_saida .= "Orig=".$i_origem.$fim;
				$xml_saida .= (strlen($i_icms)==3) ? "CSOSN=".$i_icms.$fim : "CST=".$i_icms.$fim;
			}
			else if(in_array($i_icms,$icm_ocp))
			{
				if($i_picms=='1200' || $i_picms=='1800')
				{
					$xml_saida .= "Orig=".$i_origem.$fim;
					$xml_saida .= (strlen($i_icms)==3) ? "CSOSN=".$i_icms.$fim : "CST=".$i_icms.$fim;
					$xml_saida .= "pICMS=".$i_icmsT.$fim;
				}
				else
				{
					echo "<script>Alert('Produto $i_des com imposto $i_icms e sem tributação 12% ou 18% Corrija o cadastro do produto')</script>";
				}
			}
			
			////////////////////////////////////////////////////////////////////////////////
			//////////// PIS
			$xml_saida .= $cci."PIS".$item3.$ccf.$fim;
			$pisAliq = array('01','02','05');
			$pisQtde = array('03');
			$pisSn   = array('49');
			$pisOutr = array('99');
			if(in_array($i_pis,$pisAliq))
			{
				$xml_saida .= "CST=".$i_pis.$fim.
							  "vBC=".$i_tot.$fim.
							  "pPIS=0.0165".$fim;
			}
			else if(in_array($i_pis,$pisQtde))
			{
				$xml_saida .= "CST=".$i_pis.$fim.
							  "qBCProd=".$i_qtd.$fim.
							  "vAliqProd=".number_format(($i_tot*0.0165),4,".","").$fim;
			}
			else if($i_pis=='')//PISST
			{
				$xml_saida .= "CST=".$i_pis.$fim.
							  "vBC=".$i_tot.$fim.
							  "pPIS=0.0165".$fim.
							  "qBCProd=".$i_qtd.$fim.
							  "vAliqProd=".number_format(($i_tot*0.0165),4,".","").$fim;
			}
			else if(in_array($i_pis,$pisOutr))
			{
				$xml_saida .= "CST=".$i_pis.$fim.
							  "vBC=".$i_tot.$fim.
							  "pPIS=0.0165".$fim.
							  "qBCProd=".$i_qtd.$fim.
							  "vAliqProd=".number_format(($i_tot*0.0165),4,".","").$fim;
			}
			else //SN //NT
			{
				$xml_saida .= "CST=".$i_pis.$fim;
			}
			
			////////////////////////////////////////////////////////////////////////////////
			//////////// COFINS
			$xml_saida .= $cci."COFINS".$item3.$ccf.$fim;
			$cofinsAliq = array('01','02');
			$cofinsQtde = array('03');
			$cofinsSn   = array('49');
			$cofinsOutr = array('99');
			if(in_array($i_cofins,$cofinsAliq))
			{
				$xml_saida .= "CST=".$i_cofins.$fim.
							  "vBC=".$i_tot.$fim.
							  "pCOFINS=0.076".$fim;
			}
			else if(in_array($i_cofins,$cofinsQtde))
			{
				$xml_saida .= "CST=".$i_cofins.$fim.
							  "qBCProd=".$i_qtd.$fim.
							  "vAliqProd=".number_format(($i_tot*0.076),4,".","").$fim;
			}
			else if($i_cofins=='')//COFINSST
			{
				$xml_saida .= "CST=".$i_cofins.$fim.
							  "vBC=".$i_tot.$fim.
							  "pCOFINS=0.076".$fim.
							  "qBCProd=".$i_qtd.$fim.
							  "vAliqProd=".number_format(($i_tot*0.076),4,".","").$fim;
			}
			else if(in_array($i_cofins,$cofinsOutr))
			{
				$xml_saida .= "CST=".$i_cofins.$fim.
							  "vBC=".$i_tot.$fim.
							  "pCOFINS=0.076".$fim.
							  "qBCProd=".$i_qtd.$fim.
							  "vAliqProd=".number_format(($i_tot*0.076),4,".","").$fim;
			}
			else //SN //NT
			{
				$xml_saida .= "CST=".$i_cofins.$fim;
			}
		}
		
		$v_tot  = number_format($bruto-$v_des,2,".","");
		$v12741 = (trim($PItem12741)=='') ? 0.00 : ($v_tot*$PItem12741)/100;
		
		////////////////////////////////////////////////////////////////////////////////
		//////////// FORMA DE PAGAMENTO - identificando
		$idfpnum = 0;
		$tot_pago = 0;
		$xml_pagto = "";
		$sqlFp = $this->sales_model->loadRecordVentaPago($id);
		foreach($sqlFp as $rowFp){	
			$sqlType = $this->sales_model->loadMovementsId($rowFp['movimiento_id']);
			foreach($sqlType as $rowType){
				$nameType = stripslashes($rowType['mov_detalle']);
				/*Ajuste da Forma de Pagamento conforme Sefaz*/
				if($nameType=='Dinheiro')				{	$fps = "01";	}
				else if($nameType=='Cheque')			{	$fps = "02";	}
				else if($nameType=='Cartão Crédito')	{	$fps = "03";	}
				else if($nameType=='Cartão Débito')		{	$fps = "04";	}
				else if($nameType=='Crédito Loja')		{	$fps = "05";	}
				else if($nameType=='Vale Alimentação')	{	$fps = "10";	}
				else if($nameType=='Vale Refeição')		{	$fps = "11";	}
				else if($nameType=='Vale Presente')		{	$fps = "12";	}
				else if($nameType=='Vale Combustível')	{	$fps = "13";	}
				else 									{	$fps = "99";	}
			}	
			$idfpnum ++;
			$tot_pago += $rowFp['pago_monto'];
			$idfpnum = $this->leftZero(3, $idfpnum);
			$xml_pagto .= $cci."Pagto".$idfpnum.$ccf.$fim.
						  "cMP=".$fps.$fim.
						  "vMP=".$rowFp['pago_monto'].$fim;
		}
		
		////////////////////////////////////////////////////////////////////////////////
		//////////// TOTAL / FORMA DE PAGAMENTO
		$xml_saida .= $cci."TOTAL".$ccf.$fim.
					  "vCFeLei12741=".$v12741.$fim;
		$val_d_a = $tot_pago - $v_tot;
		
		if($val_d_a != 0)
		{
			$xml_saida .= ($val_d_a>0) ? "vAcresSubtot=".$val_d_a.$fim : "vDescSubtot=".($val_d_a*-1).$fim;
		}
		$xml_saida .= $xml_pagto;
		
		////////////////////////////////////////////////////////////////////////////////
		//////////// DADOS ADICIONAIS
		$ia = (trim($infoad) != "") ? $infoad.$fim : "";
		//$ia .= "#".$id."#";
		
		$xml_saida .= $cci."DadosAdicionais".$ccf.$fim.
					  /*"infCpl=Vlr.Aprox.Trib.R$ ".$v12741." Fonte IBPT".$fim.*/
					  "infCpl=".$ia.$fim;
					  /*$cci."ObsFisco001".$ccf.$fim.
					  "xCampo=01.01.01.01".$fim.
					  "xTexto=Consulte o QRCode deste extrato atraves do App DeOlhoNaNota".$fim;*/
		
		

?>